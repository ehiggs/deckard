extern crate futures;
extern crate grpcio;
#[macro_use]
extern crate log;
extern crate log4rs;
extern crate nalgebra;
extern crate rustling_ontology;
extern crate sled;
extern crate tantivy;
extern crate telegram_bot;
extern crate tokio;

use std::env;
use std::fs::File;
use std::io::Read;
use std::process;

use futures::StreamExt;
use rustling_ontology::{build_parser, Lang, ResolverContext};
use telegram_bot::{Api, CanReplySendMessage, Error, MessageKind, UpdateKind};

struct Deckard {
    api: Api,
}

impl Deckard {
    pub fn from_config(path: &str) -> Result<Deckard, Error> {
        let mut token_file = File::open(&path).map_err(|_|format!("Could not open token file: {}", path))?;
        let mut token = String::new();
        token_file.read_to_string(&mut token).map_err(|_|"Could not read token file.")?;
        token = token.trim().to_string();
        info!("Configuring API...");
        let mut api = Api::new(token);
        Ok(Deckard { api: api })
    }
}

async fn run(args: Vec<String>) -> Result<(), Error> {
    let deckard = Deckard::from_config(&args[1])?;
    // Fetch new updates via long poll method
    let mut stream = deckard.api.stream();
    while let Some(update) = stream.next().await {
        let update = update?;
        // If the received update contains a new message...
        if let UpdateKind::Message(message) = update.kind {
            if let MessageKind::Text { ref data, .. } = message.kind {
                // Print received text message to stdout.
                info!("<{}>: {}", &message.from.first_name, data);

                let lang = Lang::EN;
                let parser = build_parser(lang).unwrap();
                let context = ResolverContext::default();
                let entities = parser.parse(data, &context).map_err(|_|"expected an entity")?;
                //.chain_err(|| "could not parse")?;

                if entities.is_empty() {
                    deckard.api.send(message.text_reply("I have nothing to say to that")).await;
                    continue;
                }

                for c in entities.iter().rev() {
                    info!("{} - {:?}", f32::exp(c.probalog), c.value);
                    deckard.api.send(message.text_reply(format!(
                        "{} - {:?}",
                        f32::exp(c.probalog),
                        c.value
                    ))).await;
                }
            }
        }
    }
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let args = env::args().collect::<Vec<_>>();
    if args.len() < 2 {
        println!("Usage: deckard <token-file>");
        process::exit(1);
    }

    if let Err(err) = log4rs::init_file("cfg/log4rs.yaml", Default::default()) {
        println!("Error configuring logger: {}", err);
        process::exit(1);
    }

    run(args).await
}
